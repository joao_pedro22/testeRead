<h1>Instalação</h1>

+ > ⚠️ Android Studio Version : 3.2, Setembro 2018.

+ > ⚠️ Usar a branch Develop para desenvolvimento.

+ > ⚠️ Para o talonário, o projeto PrinterLib deve estar ao lado da pasta Talonario.



+ Acessar menu Tools -> SDK Manager -> (buscar por )Android SDK

> ### Configurações do sdk platforms
![sdkPlatform](/uploads/fa84f72a885b1f37e6b910191a39b365/sdkPlatform.png)

> ### Configurações do sdk Tools    
![sdkTools1](/uploads/efeb85d48d2c957a54fe7c0b1945793b/sdkTools1.png)
![sdkTools2](/uploads/f34da7e74ee35540a71f18c1e55c32a5/sdkTools2.png)


<h2>Executando a aplicação</h2>

+ Importar projeto para o Android Studio
+ Caso seja necessário criar chave keystore para executar localmente:

> ´keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000´